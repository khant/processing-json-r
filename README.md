# WORKSHOP: Processing JSON Data in R

This is the code and material repository for the workshop titled "Processing JSON data in R".

## Authors and acknowledgment

- Taimur Khan - UFZ (Bzf.)

## License

[MIT](/LICENSE)

## Project status

This project is complete.
